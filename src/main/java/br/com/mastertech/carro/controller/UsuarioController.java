package br.com.mastertech.carro.controller;

import br.com.mastertech.carro.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @GetMapping
    public Usuario mostrarInfo(@AuthenticationPrincipal Usuario usuario) {
        return usuario;
    }
}